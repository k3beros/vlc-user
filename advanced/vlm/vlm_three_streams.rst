.. _vlm-broadcast-multiple:

#########################################
VLM Example: Broadcast Multiple Streams
#########################################

This page explains how to setup a ``.vlm`` file that broadcasts multiple streams. 

Broadcasting multiple streams is different from broadcasting a playlist because playlist is a single stream. When we broadcast multiple streams, the receiver(s) can choose to connect to any one of them. For a real world example, broadcasting multiple streams is like listening to the radio. The receiver can tune in to any channel and listen to it. They can also switch between channels as and when they wish.

As a pre-requisite, refer to :ref:`VLM Introduction <vlm-introduction>` to understand how to broadcast a single stream.

*************************
VLM Configuration File
*************************

For this example, we will broadcast three streams simultaneously over RTP. Refer to Stream over RTP to get a basic idea of how streaming over RTP works. 


Consider the following code for the VLM file:


.. code-block ::

   new channel_1 broadcast enabled
   setup channel_1 input "file:///C:/Users/User_name/Desktop/one.mp3"
   setup channel_1 output #rtp{mux=ts,dst=239.255.255.250,port=5010,sap,name=stream_1}
   setup channel_1 option file-caching=1000
   control channel_1 play

   new channel_2 broadcast enabled
   setup channel_2 input "file:///C:/Users/User_name/Desktop/two.mp3"
   setup channel_2 output #rtp{mux=ts,dst=239.255.255.251,port=5011,sap,name=stream_2}
   setup channel_2 option file-caching=1000
   control channel_2 play

   new channel_3 broadcast enabled
   setup channel_3 input "file:///C:/Users/User_name/Desktop/three.mp3"
   setup channel_3 output #rtp{mux=ts,dst=239.255.255.252,port=5012,sap,name=stream_3}
   setup channel_3 option file-caching=1000
   control channel_3 play

For the above code:

 + Create a new file in any text editor and copy the code.
 + Modify the file paths according to where the files are located in your system.
 + Save the file with the ``.vlm`` exension (like ``multiple.vlm``).


************************
Execute the VLM file
************************

To start the streams, open the terminal, navigate to the directory where you saved the ``.vlm`` file, and run the following command:

.. code-block ::

   $ vlc --vlm-conf=multiple.vlm


**************************
Receive the streams
**************************

All the devices that are present in the local network can simply connect to one of the SAP streams and start receiving. Simply follow these steps:

 + Open the VLC media player on the receiving device and select to :menuselection:`View -> Playlist`. 
 + Click on ``Network Streams (SAP)`` which is in the ``Local Network`` section on the left side.
 + You will see three options ``stream_1``, ``stream_2``, and ``stream_3`` (like in the figure below).
 + Double click on the one that you wish to connect to and start receiving.  


.. figure::  /images/advanced/vlm/vlm_three_streams_SAP.png
   :align:   center

   Multiple SAP streams are visible. The user can choose and switch between them.

|

If you do not see the streams in the SAP section, there is an alternate way to connect to the RTP stream directly from the terminal. Run the following code on the receiving device to connect to ``channel_1``:

.. code-block ::

   $ vlc rtp://239.255.255.250:5010

To connect to ``channel_2`` or ``channel_3``, modify the IP address and the port number according to what we defined in the ``.vlm`` file.