.. _stream-over-udp:

################################
Stream over UDP
################################

This page explains how VLC Media Player can be used to stream a file locally over UDP. Streaming locally means that the streaming device and all the receiving device(s) should be present in the same local network (so they should be connected to the same router) for the streaming to work.

To stream a media file over UDP, make the following modifications in the ``std`` block:

 + Set ``access=udp``, and
 + Set ``dst=address:port`` (where ``address`` is the IP address of the receiving device).

The destination IP address can be:

 + A unicast address (when streaming to a specific device), or 
 + A multicast address (when streaming to multiple device in the network).

You can pick any port number to stream (except for a few reserved ports). For this example, we will stream on port number ``8090``.

For this example, let's assume that the network IP addresses of the devices involved are:

 + Streaming device: 192.168.0.101
 + Receiving device(s): 192.168.0.102, 192.168.0.103, 192.168.0.104
 + Multicast address: 239.255.255.250

.. graphviz::

	digraph udp_network {
	  rankdir=TB;
	  node [shape=circle];
	  
	  { node [shape=plaintext] "Multicast IP address: 239.255.255.250"
	  }

	  { node [shape=box]; "\n Router \n 192.168.0.1 \n\n"
	  }

	 "\n Router \n 192.168.0.1 \n\n" -> "Device 2 \n 192.168.0.102"  [arrowhead=none]
	 "\n Router \n 192.168.0.1 \n\n" -> "Device 3 \n 192.168.0.103"  [arrowhead=none]
	 "Device 2 \n 192.168.0.102" -> "Multicast IP address: 239.255.255.250" [style=invis]
	 "Device 3 \n 192.168.0.103" -> "Multicast IP address: 239.255.255.250" [style=invis]


	  subgraph devices {
	  rank=same;
	  rankdir=LR;
	  "Device 1 \n 192.168.0.101" -> "\n Router \n 192.168.0.1 \n\n" -> "Device 4 \n 192.168.0.104" [arrowhead=none] [label="                                 "];
	  }

	}

|


.. _unicast-stream-over-udp:

******************************
Unicast streaming using UDP 
******************************

+++++++++++++++++++++++++
Sending a Unicast Stream
+++++++++++++++++++++++++

To start streaming, run the following code in the streaming device: 

.. code-block ::

   $ vlc sample.mp4 --sout="#std{access=udp, mux=ts, dst=192.168.0.102:8090}"

Notice that the IP address in ``dst`` is the IP address of the receiving device.

+++++++++++++++++++++++++++++
Receiving a Unicast Stream
+++++++++++++++++++++++++++++

To receive the stream, run the following code on the receiving device: 

.. code-block ::

   $ vlc udp://@:8090

Alternatively, you could run the following code which will accomplish the same thing as the one above:

.. code-block ::

   $ vlc udp://@192.168.0.102:8090


In unicast streaming over UDP, a simplified representation of the network would looks something like this:

.. graphviz::

	digraph udp_unicast {
    rankdir=TB;
    node [shape=circle];
    
    { node [width=0 shape=point label=""]; idle;
    }
    
    { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\"#std{access=udp,\nmux=ts, dst=192.168.0.102:8090}\"", "$ vlc udp://@:8090";
    }
    
    
    "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server \"pushes\" the stream to\nthe client's IP at the specified port."] 
    "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\"#std{access=udp,\nmux=ts, dst=192.168.0.102:8090}\"":w [style=invis]
    "Receiving Device \n IP: 192.168.0.102" -> "$ vlc udp://@:8090" [style=invis]
    "Idle Device \n IP: 192.168.0.103" -> "Receiving Device \n IP: 192.168.0.102" [style=invis]
    
    idle -> "Streaming Device \n IP: 192.168.0.101" [arrowhead=none]
    idle -> "Idle Device \n IP: 192.168.0.103" [arrowhead=none] [label="This device will not receive the stream because\nthe server didn't push the stream to its IP address."]

   subgraph devices {
    rank=same;
    "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
    }
    
    subgraph arrow {
    rank=same;
    idle, "Idle Device \n IP: 192.168.0.103";
    }

  }

|


.. _multicast-stream-over-udp:

******************************
Multicast streaming using UDP 
******************************

Streaming to the multicast IP address essentially means that the stream will the pushed to all the devices in the local network. So any device that wishes to receive the stream can connect to the relevant port and start receiving. 

++++++++++++++++++++++++++++
Sending a Multicast Stream
++++++++++++++++++++++++++++

To start a multicast stream, run the following code on the streaming device:

.. code-block ::

   $ vlc sample.mp4 --sout="#std{access=udp, mux=ts, dst=239.255.255.250:8090}"

Here, we have used the ``multicast`` IP address for the ``dst`` parameter. 

+++++++++++++++++++++++++++++
Receiving a Multicast Stream
+++++++++++++++++++++++++++++

To receive the stream, run the following code on all the receiving devices:

.. code-block ::

   $ vlc udp://@239.255.255.250:8090

In this case, we need to use the multicast IP address to receive the stream.

The network would might look something like the following. Notice that as udp is a ``push`` protocol, it pushes the stream to all the devices in the local network.

.. graphviz::

	digraph udp_multicast {
    rankdir=TB;
    node [shape=circle];

    { node [width=0 shape=point label=""]; two, one;
    }
    
    { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\"#std{access=udp,\nmux=ts, dst=239.255.255.250:8090}\"", "$ vlc udp://@239.255.255.250:8090\nOn the Receiving Devices.";
    }

    {node [shape=box] "\n  Network  \n  Switch  \n "
    }
    
    
    "\n  Network  \n  Switch  \n " -> "Idle Device\n IP: 192.168.0.104" [label="The other devices can\nremain idle, although,\nthe stream will still be\npushed to them in case\nthey wish to connect."] 
    "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\"#std{access=udp,\nmux=ts, dst=239.255.255.250:8090}\"":w [style=invis]
    "Idle Device\n IP: 192.168.0.104" -> "$ vlc udp://@239.255.255.250:8090\nOn the Receiving Devices." [style=invis]

    "Receiving Device 2\n IP: 192.168.0.103" -> "Idle Device\n IP: 192.168.0.104" [style=invis]
    "Receiving Device 1\n IP: 192.168.0.101" -> "Receiving Device 2\n IP: 192.168.0.103" [style=invis]
    
    one -> two [arrowhead=none]
    one -> "Receiving Device 1\n IP: 192.168.0.101" [label="The server pushes the stream\nto all the devices in the local\nnetwork at the specified port."]
    two -> "\n  Network  \n  Switch  \n " [arrowhead=none]
    two -> "Receiving Device 2\n IP: 192.168.0.103" [label="The devices that wish to\nreceive the stream can do\nso by connecting to the udp\nstream at that specific port."]

    "Streaming Device \n IP: 192.168.0.101" -> "\n  Network  \n  Switch  \n "

   subgraph devices {
    rank=same;
    "\n  Network  \n  Switch  \n ", "Streaming Device \n IP: 192.168.0.101", "Idle Device\n IP: 192.168.0.104";
    }
    
    subgraph arrow {
    rank=same;
    two, "Receiving Device 2\n IP: 192.168.0.103";
    }

    subgraph arrow2{
      rank=same;
      one, "Receiving Device 1\n IP: 192.168.0.101";
    }

  }

|

********************
Using a SAP session
********************

When streaming over UDP, you can use SAP announcements. This will help to "discover" streams on the receiving devices and seamlessly connect without having to execute any code. Refer to :ref:`Using SAP while Streaming <stream-with-sap>` to understand how to use SAP announcements.


*******************
Alternate syntax
*******************

Instead of using the ``#std{access=udp`` syntax, we can use the alternate syntax ``#udp{`` to stream over UDP. The alternate syntax is just a stortcut for calling the ``access=udp`` part of the ``std`` module, so all the other parameters remain the same.

Hence, the following code: 

.. code-block ::

   $ vlc sample.mp4 --sout="#std{access=udp, mux=ts, dst=192.168.0.102:8090}"

is equivalent to:

.. code-block ::
   
   $ vlc sample.mp4 --sout="#udp{mux=ts, dst=192.168.0.102:8090}"