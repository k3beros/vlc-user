.. _stream-over-http:

#####################
Stream over HTTP
#####################

This page explains how VLC Media Player can be used to stream a media file over HTTP in the local network. Streaming over the local network means that the streaming device and all the receiving device(s) should be connected to the same router for the streaming to work. 

To stream a media file over HTTP, do the following in the ``std`` module: 

 + Set ``access=http``, and
 + Set ``dst=:port/path`` (where ``port`` is the port number and ``path`` is the optional path name).


*********************
Sending the stream
*********************

To stream a file (``sample.mp4``) over port number ``8090`` and path name ``sample`` (you can choose your own port and path-name), the code will be:

.. code-block ::
   
   $ vlc sample.mp4 --sout="#std{access=http, mux=ts, dst=:8090/sample}"

Note: It is important to specify the ``mux`` in ``std``, otherwise the streaming fails.


.. _receive-http-stream:

*********************
Receiving the stream
*********************

As we did not mention any IP address while specifying the ``dst``, the stream will be published over the IP address of the streaming device.

As an example, let us assume that the IP address of the streaming device is ``192.168.0.101``. 

To receive the stream on the same device, you can do any of the following:

 + Run ``$ vlc http://:8090/sample``.
 + Run ``$ vlc http://localhost:8090/sample``. 
 + Run ``$ vlc http://127.0.0.1:8090/sample``.
 + Run ``$ vlc http://192.168.0.101:8090/sample``.

To receive the stream on a different device, we need to specify the IP address of the streaming device. Hence, run the following code on a new terminal in the receiving devices:

.. code-block ::

   $ vlc http://192.168.0.101:8090/sample

Diagramtically, this is how the network might look. Notice that as ``http`` is a pull protocol, the stream is sent to a device only when it requests for it.

.. graphviz::

	digraph http {
      rankdir=TB;
      node [shape=circle];
      
      { node [width=0 shape=point label=""]; idle;
      }
      
      { node [shape=plaintext]; "$ vlc sample.mp4 --sout=\"#std{access=http,\nmux=ts, dst=:8090/sample}\"",  "$ vlc http://192.168.0.101:8090/sample";
      }
      
      
      "Receiving Device \n IP: 192.168.0.102" -> "Streaming Device \n IP: 192.168.0.101" [label="The receiving device requests\nfor the stream."]
      "Streaming Device \n IP: 192.168.0.101" -> "Receiving Device \n IP: 192.168.0.102" [label="The server then sends the\nstream to the client's IP\nat the specified port."] 
      


      "Streaming Device \n IP: 192.168.0.101":w -> "$ vlc sample.mp4 --sout=\"#std{access=http,\nmux=ts, dst=:8090/sample}\"":w [style=invis]
      "Receiving Device \n IP: 192.168.0.102" -> "$ vlc http://192.168.0.101:8090/sample" [style=invis]
      "Idle Device \n IP: 192.168.0.103" -> "Receiving Device \n IP: 192.168.0.102" [style=invis]
      
      idle -> "Streaming Device \n IP: 192.168.0.101" [arrowhead=none]
      idle -> "Idle Device \n IP: 192.168.0.103" [arrowhead=none] [label="This device is idle because it has not\nrequested for the stream. Although, it can\nsend a request any time and begin receiving."]

     subgraph devices {
      rank=same;
      "Streaming Device \n IP: 192.168.0.101", "Receiving Device \n IP: 192.168.0.102";
      }
      
      subgraph arrow {
      rank=same;
      idle, "Idle Device \n IP: 192.168.0.103";
      }

    }


|

In case of streaming over HTTP, the ``access output`` part (at the end of the sout pipeline) works a little differently than in other cases. This is because in case of HTTP-streaming, the data isn't sent anywhere until a client machine requests for it, while in other scenarios (like when streaming over UDP, or when saving the file), the data is "pushed" to relevant address specified through the ``dst`` parameter. 


*******************
Alternate syntax
*******************

Instead of using the ``#std{access=http`` syntax, we can use the alternate syntax ``#http{`` to stream over HTTP. The alternate syntax is just a stortcut for calling the ``access=http`` part of the ``std`` module, so all the other parameters remain the same.

Hence, the following code: 

.. code-block ::

   $ vlc sample.mp4 --sout="#std{access=http, mux=ts, dst=:8090/sample}"

is equivalent to:

.. code-block ::
   
   $ vlc sample.mp4 --sout="#http{mux=ts, dst=:8090/sample}"