################################
 Play multiple instances of VLC
################################

This page describes how to play multiple instances of the VLC Media Player in Windows, macOS, and Linux based systems.

*******
Windows 
*******

=================
Graphical method
=================
To play multiple instances of VLC in Windows, follow these steps:

 + Open the VLC Media Player.
 + Go to :menuselection:`Tools --> Preferences` (Keyboard shortcut is Ctrl+P).
 + Select the :guilabel:`Interface` tab and scroll down to ``Playlists and Instances``.
 + Uncheck the :guilabel:`Allow only one instance` option.
 + Uncheck the :guilabel:`Use only one instance when started from file manager` option.
 + Click on the :guilabel:`Save` button.

.. figure::  /images/advanced/player/play_multiple_instances_windows.png
   :align:   center

   Notice the unchecked boxes in the Playlists and Instances section.


====================
Command Line method
====================
Multiple instances can be opened using the `START <https://ss64.com/nt/start.html>`_ command.

Suppose you wish to play files ``file1.mp3`` and ``file2.ogg`` simultaneously in two different instances of VLC. You can do that by following these steps: 

 + Open the terminal.
 + Navigate to the directory where the files are stored.
 + Run the following command:

.. code-block::

	> START "VLC Instance 1" "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" file1.mp3 --no-one-instance && START "VLC Instance 2" "C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" file2.ogg --no-one-instance



*******
macOS 
*******
On macOS, running multiple instances is not directly supported. As a workaround, it can be done by creating a VLC Droplet/App by following these steps:

 + Open the ``Script Editor`` from :guilabel:`Finder`.
 + Create a ``New Document`` and paste the following code:

 .. code-block::

	on run
    	do shell script "open -n /Applications/VLC.app"
    	tell application "VLC" to activate
	end run

	on open theFiles
	    repeat with theFile in theFiles
	        do shell script "open -na /Applications/VLC.app " & quote & (POSIX path of theFile) & quote
	    end repeat
	    tell application "VLC" to activate
	end open

 + Save it as an application.



The VLC droplet/app can be used in different ways to open multiple instances of VLC media player. You can either:

 + Launch multiple instances of the VLC droplet/app and drop a different file in each, or
 + Drop media files on the VLC droplet/app and each will open in a separate window, or
 + Associate media files with the VLC droplet/app. This way when the files are clicked, they will open in standalone windows. To do this, follow these steps:

	+ Open :guilabel:`Finder` and find the video file to be played.
	+ Right click on the file (assuming you have right click enabled).
	+ Choose ``Get Info``.
	+ Click dropdown under ``Open with:``.
	+ Select the VLC droplet/app.
	+ Click the :guilabel:`Change All` button.
	+ Select :guilabel:`Yes` if prompted "are you sure".


********************
Linux-based systems
********************
You can run multiple instances of VLC using the ``--no-one-instance`` option in linux systems. Simply open the terminal and run the following command for running ``file1.mp3`` and ``file2.ogg`` simultaneously:

.. code-block::

	$ vlc --no-one-instance file1.mp3 & vlc --no-one-instance file2.ogg