.. _getting_support:

********
Get Help
********

VLC is a free and open source software which means that most developers are volunteers. Therefore, please remember that every user support is provided by volunteers doing it in their free time. If you have problems that has not been answered on our :ref:`Frequently Asked Questions page <faq>`, you can reach out to us diretly through any of the methods listed below;

**IRC**

The `VideoLAN IRC <https://wiki.videolan.org/IRC/#IRCIRC>`_ channel is a good way to contact and speak with the team. Using an IRC client, connect to irc.videolan.org or irc.libera.chat and go on the #videolan channel. If you are in a location without direct IRC access, please try the `Webchat <https://kiwiirc.com/nextclient/#ircs://irc.libera.chat/#videolan>`_.

**Forum**

Search or ask your question on the `VideoLAN support forum <https://forum.videolan.org/>`_ 

**Administrative Contact**

Legal and administrative contacts can be found :doc:`here </lore/videolan/community/contact>` (not for support)

